﻿#include <iostream>
#include "Person.h"
#include "Vector.h"

using namespace std;

int main()
{
    Person Man(20, "Polkopkl");
    Vector V(3, 4, 0);

    cout << "Person with Name " << Man.Name() << " and Age " << Man.Age() << " years\n";

    V.Show();
    cout << "\n" << "Vector Module = " << V.Module();
}