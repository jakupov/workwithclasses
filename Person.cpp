#include "Person.h"
#include <string>

Person::Person() : age(0), name("")
{

}

Person::Person(int _age, std::string _name) : age(_age), name(_name)
{

}

int Person::Age(int _age)
{
    age = _age;
    return age;
}

int Person::Age()
{
    return age;
}

std::string Person::Name(std::string _name)
{
    name = _name;
    return name;
}

std::string Person::Name()
{
    return name;
}