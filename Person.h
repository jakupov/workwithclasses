#pragma once
#include <string>

class Person
{
private:
    int age;
    std::string name;
public:
    Person();
    Person(int _age, std::string _name);
    int Age(int _age);
    int Age();
    std::string Name(std::string _name);
    std::string Name();
};